# CheapestBookFinder

API for finding cheapest book on shopping sites. Finding is based on book cost and shipping cost, and optionally on currency exchange rate. Project based on Thorntail server. Tested on Windows system

### Prerequisites
```
Maven - Dependency management
eBay AppID - For eBay search
```

## Getting started

Add Thorntail respository to your Maven settings file (it's under ```/Users/youraccountname/.m2/```)
If you don't have this file here is a working example
```
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0
                      https://maven.apache.org/xsd/settings-1.0.0.xsd">
  <localRepository/>
  <interactiveMode/>
  <usePluginRegistry/>
  <offline/>
  <pluginGroups/>
  <servers />
  <mirrors />
  <proxies/>
  <profiles>
  <profile>
     <id>myprofile</id>
     <repositories>
       <repository>
         <id>sonatype</id>
         <name>sonatype</name>
         <url>https://oss.sonatype.org/content/repositories/snapshots</url>
       </repository>
     </repositories>
   </profile>
  </profiles>
  <activeProfiles>
   <activeProfile>myprofile</activeProfile>
 </activeProfiles>
</settings>
```

You need to add your own eBay AppID in
```
/src/main/resources/META-INF/application.properties
```

under ``` ebay_api_key ```

Then run maven command in root directory

```
mvn clean package
```

After that execute file
```
/target/cheapest-book-finder-1.0-SNAPSHOT-bin/bin/run.bat
```

Server will start at
```
localhost:8080
```

## Testing

Tests are basen on JUnit

# Working with API

## Performing a search

You can perform search, by sending an HTTP ```GET``` request to
```
localhost:8080/search-cheapest?title=Book title
```

This request has a single required parameter:

 ```title``` - search for book with that exact title

### Request
Here is an example of searching for Lokatorka by JP Delaney 
```
http://localhost:8080/search-cheapest?title=Lokatorka
```

### Response
If the request succeds, the server response with a ```200``` status code and search result:
```
{  
   "status":200,
   "bookUrl":"https://play.google.com/store/books/details?id=3Ls9DwAAQBAJ&rdid=book-3Ls9DwAAQBAJ&rdot=1&source=gbs_api"
}
```

### Optional parameter
``` currency ``` - when provided it will search by this currency and it's exchange rates

##### Available currencies
PLN, GBP, EUR, USD, CAD

### Request
Here is an example of searching for Lokatorka by JP Delaney basen on Polish zloty
```
http://localhost:8080/search-cheapest?title=Lokatorka&currency=PLN
```
### Response
If the request succeds, the server response with a ```200``` status code and search result:
```
{  
   "status":200,
   "bookUrl":"https://play.google.com/store/books/details?id=3Ls9DwAAQBAJ&rdid=book-3Ls9DwAAQBAJ&rdot=1&source=gbs_api"
}
```

### Error and info messages
There are 4 types of result messages in addition to successful one

#### 204.1 Not found any books by given title
```
{  
   "status":204,
   "message":{  
      "code":1,
      "message":"Not found any books by given title"
   }
}
```
It's when a server could not resolve book title.

#### 204.2 Not found any books for sale
```
{  
   "status":204,
   "message":{  
      "code":2,
      "message":"Not found any books for sale"
   }
}
```
It's when a server resolved title of the book, but could't find it for sale

#### 400.1 Not found any books for sale
```
{  
   "status":400,
   "message":{  
      "code":1,
      "message":"Required parameter title is empty"
   }
}
```
It's when a title parameter wasn't present on request

#### 406.2 Not found any books for sale
```
{  
   "status":406,
   "message":{  
      "code":2,
      "message":"Not known currency"
   }
}
```
It's when a provided currency is not from available currencies



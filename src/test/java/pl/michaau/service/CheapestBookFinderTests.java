package pl.michaau.service;

import com.google.common.collect.Sets;
import org.junit.Ignore;
import org.junit.Test;
import pl.michaau.domain.AmountOfMoney;
import pl.michaau.domain.CheapestBookToFindParameters;
import pl.michaau.domain.FoundBook;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Optional;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class CheapestBookFinderTests {

	@Test
	public void findingCheapestBookWithNoCurrencyConversion(){
		CheapestBookFinderService finderService = new CheapestBookFinderService(CheapestBookToFindParameters.builder()
				.title(Optional.of("title"))
				.currency(Optional.ofNullable("PLN"))
				.build());
		FoundBook cheapestBook = new FoundBook("title", new AmountOfMoney(BigDecimal.ZERO, Currency.getInstance("PLN")));
		Set<FoundBook> books = Sets.newHashSet(
				new FoundBook("title", new AmountOfMoney(BigDecimal.ONE, Currency.getInstance("PLN"))),
				new FoundBook("title", new AmountOfMoney(BigDecimal.TEN, Currency.getInstance("PLN"))),
				new FoundBook("title", new AmountOfMoney(BigDecimal.valueOf(0.1), Currency.getInstance("PLN"))),
				new FoundBook("title", new AmountOfMoney(BigDecimal.valueOf(15897654), Currency.getInstance("PLN")))
		);
		books.add(cheapestBook);
		assertThat(finderService.findCheapest(books),is(cheapestBook));
	}

	@Test
	@Ignore("CheapestBookFinderTests.findingCheapestBookWithCurrencyConversion: Test depends if currency provider is up")
	public void findingCheapestBookWithCurrencyConversion(){
		CheapestBookFinderService finderService = new CheapestBookFinderService(CheapestBookToFindParameters.builder()
				.title(Optional.of("title"))
				.currency(Optional.ofNullable("USD"))
				.build());
		FoundBook cheapestBook = new FoundBook("title", new AmountOfMoney(BigDecimal.ZERO, Currency.getInstance("USD")));
		Set<FoundBook> books = Sets.newHashSet(
				new FoundBook("title", new AmountOfMoney(BigDecimal.ONE, Currency.getInstance("EUR"))),
				new FoundBook("title", new AmountOfMoney(BigDecimal.TEN, Currency.getInstance("GBP"))),
				new FoundBook("title", new AmountOfMoney(BigDecimal.valueOf(0.1), Currency.getInstance("PLN"))),
				new FoundBook("title", new AmountOfMoney(BigDecimal.valueOf(15897654), Currency.getInstance("PLN")))
		);
		books.add(cheapestBook);
		assertThat(finderService.findCheapest(books),is(cheapestBook));
	}

}

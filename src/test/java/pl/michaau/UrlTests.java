package pl.michaau;

import io.thorntail.test.ThorntailTestRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.is;

@RunWith(ThorntailTestRunner.class)
public class UrlTests {

	@Test
	public void rootUrl() {
		when().get("/").then()
				.statusCode(404);

	}

	@Test
	public void noParameters() {
		when().get("/search-cheapest").then()
				.statusCode(200)
				.body(is("{\"status\":400,\"message\":{\"code\":1,\"message\":\"Required parameter title is empty\"}}"));
	}

	@Test
	public void emptyTitleParameter() {
		when().get("/search-cheapest?title=").then()
				.statusCode(200)
				.body(is("{\"status\":400,\"message\":{\"code\":1,\"message\":\"Required parameter title is empty\"}}"));
	}

	@Test
	public void wrongCurrencyParameter() {
		when().get("/search-cheapest?title=ZZZ&currency=UUU").then()
				.statusCode(200)
				.body(is("{\"status\":406,\"message\":{\"code\":2,\"message\":\"Not known currency\"}}"));
	}
}
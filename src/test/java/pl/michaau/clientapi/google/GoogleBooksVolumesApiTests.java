package pl.michaau.clientapi.google;

import org.hamcrest.collection.IsEmptyCollection;
import org.junit.Ignore;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class GoogleBooksVolumesApiTests {

	@Test
	@Ignore("GoogleBooksVolumesApiTests.findIsbnsOfKnownBookByTitle: You need to go first to google books page and copy first book title and paste it as parameter to findIsbnsByQuery method, then you can run this test")
	public void findIsbnsOfKnownBookByTitle() {
		assertThat(new GoogleBooksVolumesApi().findIsbnsByQuery("Harry Potter i komnata tajemnic"), not(IsEmptyCollection.empty()));
	}

	@Test
	@Ignore("GoogleBooksVolumesApiTests.findKnownBookForSaleByIsbn: You need to go first to google books page and find book with ISBN number for sale and then paste it as parameter to findBooksByIsbn method, then you can run this test")
	public void findKnownBookForSaleByIsbn() {
		assertThat(new GoogleBooksVolumesApi().findBooksByIsbn("9781781104224"), not(IsEmptyCollection.empty()));
	}

}

package pl.michaau.clientapi.google;

import org.junit.Test;
import pl.michaau.clientapi.google.domain.SearchByIsbnResponse;
import pl.michaau.clientapi.google.domain.SearchByQueryResponse;
import pl.michaau.clientapi.utils.Mapper;
import pl.michaau.domain.FoundBook;

import java.io.IOException;
import java.util.Set;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class GoogleBooksVolumesJsonTests {

	@Test
	public void emptyJsonForSearchByQueryResponse() throws IOException {
		Set<String> isbns = Mapper.map("{}", SearchByQueryResponse.class).isbnsOf("Title");
		assertTrue(isbns.isEmpty());
	}

	@Test
	public void jsonWithoutItemsForSearchByQueryResponse() throws IOException {
		Set<String> isbns = Mapper.map("{\"items\":[]}", SearchByQueryResponse.class).isbnsOf("Title");
		assertTrue(isbns.isEmpty());
	}

	@Test
	public void jsonWithEmptyVolumeInfoForSearchByQueryResponse() throws IOException {
		Set<String> isbns = Mapper.map("{\"items\":[{\"volumeInfo\":{}}]}", SearchByQueryResponse.class).isbnsOf("Title");
		assertTrue(isbns.isEmpty());
	}

	@Test
	public void jsonWithEmptyIndustryIdentifiersForSearchByQueryResponse() throws IOException {
		Set<String> isbns = Mapper.map("{\"items\":[{\"volumeInfo\":{\"industryIdentifiers\":[{}]}}]}", SearchByQueryResponse.class).isbnsOf("Title");
		assertTrue(isbns.isEmpty());
	}

	@Test
	public void jsonWithSameIndustryIdentifiersForSearchByQueryResponse() throws IOException {
		assertThat(Mapper.map("{\"items\":[{\"volumeInfo\":{\"title\":\"Title\",\"industryIdentifiers\":[{\"type\":\"ISBN_10\",\"identifier\":\"8302097209\"},{\"type\":\"ISBN_13\",\"identifier\":\"9788302097201\"},{\"type\":\"ISBN_10\",\"identifier\":\"8302097209\"},{\"type\":\"ISBN_13\",\"identifier\":\"9788302097201\"}]}}]}", SearchByQueryResponse.class).isbnsOf("Title"), hasSize(2));
	}

	@Test
	public void correctJsonForSearchByQueryResponse() throws IOException {
		assertThat(Mapper.map("{\"items\":[{\"volumeInfo\":{\"title\":\"Title\",\"industryIdentifiers\":[{\"type\":\"ISBN_10\",\"identifier\":\"8302097209\"},{\"type\":\"ISBN_13\",\"identifier\":\"9788302097201\"}]}},{\"volumeInfo\":{\"title\":\"Naukaczytania i pisania\",\"industryIdentifiers\":[{\"type\":\"OTHER\",\"identifier\":\"UIUC:30112119326160\"}]}}]}", SearchByQueryResponse.class).isbnsOf("Title"), hasSize(2));
	}

	@Test
	public void emptyJsonForSearchByIsbnResponse() throws IOException {
		Set<FoundBook> books = Mapper.map("{}", SearchByIsbnResponse.class).getBooks();
		assertTrue(books.isEmpty());
	}

	@Test
	public void jsonWithoutItemsForSearchByIsbnResponse() throws IOException {
		Set<FoundBook> books = Mapper.map("{\"items\":[]}", SearchByIsbnResponse.class).getBooks();
		assertTrue(books.isEmpty());
	}

	@Test
	public void jsonWithEmptySaleInfoForSearchByIsbnResponse() throws IOException {
		Set<FoundBook> books = Mapper.map("{\"items\":[{\"saleInfo\":{}}]}", SearchByIsbnResponse.class).getBooks();
		assertTrue(books.isEmpty());
	}

	@Test
	public void jsonWithEmptyRetailPriceByRestIsCorrectForSearchByIsbnResponse() throws IOException {
		Set<FoundBook> books = Mapper.map("{\"items\":[{\"saleInfo\":{\"retailPrice\":{},\"buyLink\":\"https://play.google.com/store/books/details?id=Kw8bDQAAQBAJ&rdid=book-Kw8bDQAAQBAJ&rdot=1&source=gbs_api\"}}]}", SearchByIsbnResponse.class).getBooks();
		assertTrue(books.isEmpty());
	}

	@Test
	public void jsonWithNoBuyLinkButRestIsCorrectForSearchByIsbnResponse() throws IOException {
		Set<FoundBook> books = Mapper.map("{\"items\":[{\"saleInfo\":{\"retailPrice\":{\"amount\":39.99,\"currencyCode\":\"PLN\"}}}]}", SearchByIsbnResponse.class).getBooks();
		assertTrue(books.isEmpty());
	}

	@Test
	public void jsonWithNoRetailPriceButRestIsCorrectForSearchByIsbnResponse() throws IOException {
		Set<FoundBook> books = Mapper.map("{\"items\":[{\"saleInfo\":{\"retailPrice\":{\"amount\":39.99,\"currencyCode\":\"PLN\"}}}]}", SearchByIsbnResponse.class).getBooks();
		assertTrue(books.isEmpty());
	}

	@Test
	public void jsonWithNoCurrencyButRestIsCorrectForSearchByIsbnResponse() throws IOException {
		Set<FoundBook> books = Mapper.map("{\"items\":[{\"saleInfo\":{\"retailPrice\":{\"amount\":39.99},\"buyLink\":\"https://play.google.com/store/books/details?id=Kw8bDQAAQBAJ&rdid=book-Kw8bDQAAQBAJ&rdot=1&source=gbs_api\"}}]}", SearchByIsbnResponse.class).getBooks();
		assertTrue(books.isEmpty());
	}

	@Test
	public void jsonWithNoAmountButRestIsCorrectForSearchByIsbnResponse() throws IOException {
		Set<FoundBook> books = Mapper.map("{\"items\":[{\"saleInfo\":{\"retailPrice\":{\"currencyCode\":\"PLN\"},\"buyLink\":\"https://play.google.com/store/books/details?id=Kw8bDQAAQBAJ&rdid=book-Kw8bDQAAQBAJ&rdot=1&source=gbs_api\"}}]}", SearchByIsbnResponse.class).getBooks();
		assertTrue(books.isEmpty());
	}

}

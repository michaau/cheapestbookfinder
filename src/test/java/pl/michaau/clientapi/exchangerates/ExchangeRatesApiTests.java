package pl.michaau.clientapi.exchangerates;

import org.junit.Ignore;
import org.junit.Test;
import pl.michaau.domain.AmountOfMoney;
import pl.michaau.domain.AvailableCurrency;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class ExchangeRatesApiTests {

	@Test
	@Ignore("ExchangeRatesApiTests.checkAvailableCurrencies: Test depends if currency provider is up")
	public void checkAvailableCurrencies(){
		Currency plnCurrency = Currency.getInstance("PLN");
		Set<Currency> expected = AvailableCurrency.getAllWithout(plnCurrency).stream().map(AvailableCurrency::getCurrency).collect(Collectors.toSet());
		Map<Currency, BigDecimal> rates = new ExchangeRatesApi(plnCurrency).getRates();
		assertThat(rates.keySet(), containsInAnyOrder(expected.toArray()));
	}

	@Test
	@Ignore("ExchangeRatesApiTests.amountConvert: Test depends if currency provider is up")
	public void amountConvert(){
		AmountOfMoney amountOfMoney = new AmountOfMoney(new BigDecimal(40), Currency.getInstance("PLN")).convertTo(Currency.getInstance("USD"));
		assertThat(amountOfMoney, is(notNullValue()));
		assertThat(amountOfMoney.getAmount(), is(notNullValue()));
		assertThat(amountOfMoney.getCurrency(), is(notNullValue()));
	}
}

package pl.michaau.clientapi.exchangerates;

import com.google.common.collect.Maps;
import org.junit.Test;
import pl.michaau.clientapi.exchangerates.domain.ExchangeRatesResponse;
import pl.michaau.clientapi.utils.Mapper;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class ExchangeRatesJsonTests {
	@Test
	public void emptyJsonForExchangeRatesResponse() throws IOException {
		assertThat(Mapper.map("{}", ExchangeRatesResponse.class).getRatesMap(), is(new HashMap()));
	}

	@Test
	public void correctJsonForExchangeRatesResponse() throws IOException {
		HashMap<Currency, BigDecimal> rates = Maps.newHashMap();
		rates.put(Currency.getInstance("CAD"), new BigDecimal("0.3535002912"));
		rates.put(Currency.getInstance("GBP"), new BigDecimal("0.2063832266"));
		rates.put(Currency.getInstance("USD"), new BigDecimal("0.2741758882"));
		rates.put(Currency.getInstance("EUR"), new BigDecimal("0.2329644729"));
		assertThat(Mapper.map("{\"base\":\"PLN\",\"date\":\"2018-09-20\",\"rates\":{\"CAD\":0.3535002912,\"GBP\":0.2063832266,\"USD\":0.2741758882,\"EUR\":0.2329644729}}", ExchangeRatesResponse.class).getRatesMap(), is(rates));
	}

	@Test
	public void jsonWithEmptyRatesForExchangeRatesResponse() throws IOException {
		Map<Currency, BigDecimal> ratesMap = Mapper.map("{\"rates\":{}}", ExchangeRatesResponse.class).getRatesMap();
		assertTrue(ratesMap.isEmpty());
	}

	@Test
	public void jsonWithNullValueInRateValueForExchangeRatesResponse() throws IOException {
		Map<Currency, BigDecimal> ratesMap = Mapper.map("{\"rates\":{\"CAD\":null,\"GBP\":0.2063832266,\"USD\":0.2741758882,\"EUR\":0.2329644729}}", ExchangeRatesResponse.class).getRatesMap();
		assertTrue(ratesMap.isEmpty());
	}

	@Test
	public void jsonWithNullValueInCurrencyValueForExchangeRatesResponse() throws IOException {
		Map<Currency, BigDecimal> ratesMap = Mapper.map("{\"rates\":{\"null\":3,\"GBP\":0.2063832266,\"USD\":0.2741758882,\"EUR\":0.2329644729}}", ExchangeRatesResponse.class).getRatesMap();
		assertTrue(ratesMap.isEmpty());
	}

}

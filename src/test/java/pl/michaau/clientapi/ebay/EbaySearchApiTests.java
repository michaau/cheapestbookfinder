package pl.michaau.clientapi.ebay;

import org.hamcrest.collection.IsEmptyCollection;
import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

public class EbaySearchApiTests {

	@Test
	@Ignore("EbaySearchByProductIsbnApiTests.findKnownBookForSaleByIsbn: You need to go first to ebay page and find book with ISBN number and then paste it as parameter to findBooksByIsbn method, then you can run this test")
	public void findKnownBookForSaleByIsbn() {
		assertThat(new EbaySearchApi().findBooksByIsbn("9781501180989"), not(IsEmptyCollection.empty()));
	}

}

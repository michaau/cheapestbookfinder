package pl.michaau.clientapi.ebay;

import org.junit.Test;
import pl.michaau.clientapi.ebay.domain.Response;
import pl.michaau.clientapi.utils.Mapper;
import pl.michaau.domain.FoundBook;

import java.io.IOException;
import java.util.Set;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class EbaySearchJsonTests {

	@Test
	public void correctJsonForEbayResponse() throws IOException {
		assertThat(Mapper.map("{\"findItemsByProductResponse\":[{\"searchResult\":[{\"item\":[{\"viewItemURL\":[\"http:\\/\\/www.ebay.com\\/itm\\/Calypso-David-Sedaris-2018-Hardcover-\\/232931992269\"],\"shippingInfo\":[{\"shippingServiceCost\":[{\"@currencyId\":\"USD\",\"__value__\":\"0.0\"}]}],\"sellingStatus\":[{\"currentPrice\":[{\"@currencyId\":\"USD\",\"__value__\":\"12.0\"}]}]}]}]}]}", Response.class).getBooks(), hasSize(1));
	}

	@Test
	public void emptyJsonForEbayResponse() throws IOException {
		Set<FoundBook> books = Mapper.map("{}", Response.class).getBooks();
		assertTrue(books.isEmpty());
	}

	@Test
	public void jsonWithEmptyFindItemsByProductResponse() throws IOException {
		Set<FoundBook> books = Mapper.map("{\"findItemsByProductResponse\":[]}", Response.class).getBooks();
		assertTrue(books.isEmpty());
	}

	@Test
	public void jsonWithEmptySearchResult() throws IOException {
		Set<FoundBook> books = Mapper.map("{\"findItemsByProductResponse\":[{\"searchResult\":[]}]}", Response.class).getBooks();
		assertTrue(books.isEmpty());
	}

	@Test
	public void jsonWithEmptyItem() throws IOException {
		Set<FoundBook> books = Mapper.map("{\"findItemsByProductResponse\":[{\"searchResult\":[{\"item\":[]}]}]}", Response.class).getBooks();
		assertTrue(books.isEmpty());
	}

	@Test
	public void jsonWithEmptyViewItemUrlButRestIsCorrect() throws IOException {
		Set<FoundBook> books = Mapper.map("{\"findItemsByProductResponse\":[{\"searchResult\":[{\"item\":[{\"shippingInfo\":[{\"shippingServiceCost\":[{\"@currencyId\":\"USD\",\"__value__\":\"0.0\"}]}],\"sellingStatus\":[{\"currentPrice\":[{\"@currencyId\":\"USD\",\"__value__\":\"12.0\"}]}]}]}]}]}", Response.class).getBooks();
		assertTrue(books.isEmpty());
	}

	@Test
	public void jsonWithEmptyShippingInfoButRestIsCorrect() throws IOException {
		Set<FoundBook> books = Mapper.map("{\"findItemsByProductResponse\":[{\"searchResult\":[{\"item\":[{\"viewItemURL\":[\"http:\\/\\/www.ebay.com\\/itm\\/Calypso-David-Sedaris-2018-Hardcover-\\/232931992269\"],\"sellingStatus\":[{\"currentPrice\":[{\"@currencyId\":\"USD\",\"__value__\":\"12.0\"}]}]}]}]}]}", Response.class).getBooks();
		assertTrue(books.isEmpty());
	}

	@Test
	public void jsonWithEmptySellingStatusButRestIsCorrect() throws IOException {
		Set<FoundBook> books = Mapper.map("{\"findItemsByProductResponse\":[{\"searchResult\":[{\"item\":[{\"viewItemURL\":[\"http:\\/\\/www.ebay.com\\/itm\\/Calypso-David-Sedaris-2018-Hardcover-\\/232931992269\"],\"shippingInfo\":[{\"shippingServiceCost\":[{\"@currencyId\":\"USD\",\"__value__\":\"0.0\"}]}]}]}]}]}", Response.class).getBooks();
		assertTrue(books.isEmpty());
	}

	@Test
	public void jsonWithEmptyShippingServiceCostButRestIsCorrect() throws IOException {
		Set<FoundBook> books = Mapper.map("{\"findItemsByProductResponse\":[{\"searchResult\":[{\"item\":[{\"viewItemURL\":[\"http:\\/\\/www.ebay.com\\/itm\\/Calypso-David-Sedaris-2018-Hardcover-\\/232931992269\"],\"shippingInfo\":[{\"shippingServiceCost\":[]}],\"sellingStatus\":[{\"currentPrice\":[{\"@currencyId\":\"USD\",\"__value__\":\"12.0\"}]}]}]}]}]}", Response.class).getBooks();
		assertTrue(books.isEmpty());
	}

	@Test
	public void jsonWithNoCurrencyIdInShippingServiceCostButRestIsCorrect() throws IOException {
		Set<FoundBook> books = Mapper.map("{\"findItemsByProductResponse\":[{\"searchResult\":[{\"item\":[{\"viewItemURL\":[\"http:\\/\\/www.ebay.com\\/itm\\/Calypso-David-Sedaris-2018-Hardcover-\\/232931992269\"],\"shippingInfo\":[{\"shippingServiceCost\":[{\"__value__\":\"0.0\"}]}],\"sellingStatus\":[{\"currentPrice\":[{\"@currencyId\":\"USD\",\"__value__\":\"12.0\"}]}]}]}]}]}", Response.class).getBooks();
		assertTrue(!books.isEmpty());
	}

	@Test
	public void jsonWithNoValueInShippingServiceCostButRestIsCorrect() throws IOException {
		Set<FoundBook> books = Mapper.map("{\"findItemsByProductResponse\":[{\"searchResult\":[{\"item\":[{\"viewItemURL\":[\"http:\\/\\/www.ebay.com\\/itm\\/Calypso-David-Sedaris-2018-Hardcover-\\/232931992269\"],\"shippingInfo\":[{\"shippingServiceCost\":[{\"@currencyId\":\"USD\"}]}],\"sellingStatus\":[{\"currentPrice\":[{\"@currencyId\":\"USD\",\"__value__\":\"12.0\"}]}]}]}]}]}", Response.class).getBooks();
		assertTrue(books.isEmpty());
	}

	@Test
	public void jsonWithNoCurrencyInCurrentPriceCostButRestIsCorrect() throws IOException {
		Set<FoundBook> books = Mapper.map("{\"findItemsByProductResponse\":[{\"searchResult\":[{\"item\":[{\"viewItemURL\":[\"http:\\/\\/www.ebay.com\\/itm\\/Calypso-David-Sedaris-2018-Hardcover-\\/232931992269\"],\"shippingInfo\":[{\"shippingServiceCost\":[{\"@currencyId\":\"USD\",\"__value__\":\"0.0\"}]}],\"sellingStatus\":[{\"currentPrice\":[{\"__value__\":\"12.0\"}]}]}]}]}]}", Response.class).getBooks();
		assertTrue(books.isEmpty());
	}

	@Test
	public void jsonWithNoValueInCurrentPriceCostButRestIsCorrect() throws IOException {
		Set<FoundBook> books = Mapper.map("{\"findItemsByProductResponse\":[{\"searchResult\":[{\"item\":[{\"viewItemURL\":[\"http:\\/\\/www.ebay.com\\/itm\\/Calypso-David-Sedaris-2018-Hardcover-\\/232931992269\"],\"shippingInfo\":[{\"shippingServiceCost\":[{\"@currencyId\":\"USD\",\"__value__\":\"0.0\"}]}],\"sellingStatus\":[{\"currentPrice\":[{\"@currencyId\":\"USD\"}]}]}]}]}]}", Response.class).getBooks();
		assertTrue(books.isEmpty());
	}
}

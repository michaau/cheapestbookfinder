package pl.michaau.service;

import com.google.common.base.Strings;
import pl.michaau.domain.*;

import javax.ws.rs.core.Response;
import java.util.*;

public class CheapestBookFinderService {

	private static final long INFO_BOOKS_NOT_FOUND_BY_TITLE = 1L;
	private static final long INFO_NOT_FOUND_CHEAPEST_BOOK = 2L;

	private static final long ERROR_NO_TITLE = 1L;
	private static final long ERROR_NOT_KNOWN_CURRENCY = 2L;

	private CheapestBookToFindParameters parameters;

	public CheapestBookFinderService(CheapestBookToFindParameters parameters) {
		this.parameters = parameters;
	}

	public CheapestDealResponse find() {
		if (wrongParameters()) {
			return wrongParametersResponse();
		}
		BookIsbnFinderService isbnFinder = new BookIsbnFinderService();
		Set<String> isbns = isbnFinder.findByTitle(parameters.getTitle().get());
		if (isbns.isEmpty()) {
			return booksByGivenTitleNotFoundResponse();
		}
		BooksForSaleFinderService searcher = new BooksForSaleFinderService();
		Set<FoundBook> foundBooks = searcher.findByIsbns(isbns);
		if (foundBooks.isEmpty()) {
			return bookForSaleNotFoundResponse();
		}
		return new CheapestDealResponse(findCheapest(foundBooks).getBookLink());
	}

	FoundBook findCheapest(Set<FoundBook> foundBooks) {
		Currency currency = getCurrency();
		Set<FoundBook> returned = new HashSet<>();
		for (FoundBook foundBook : foundBooks) {
			if (!foundBook.getAmountOfMoney().getCurrency().equals(currency)) {
				returned.add(new FoundBook(foundBook.getBookLink(), foundBook.getAmountOfMoney().convertTo(currency)));
			} else {
				returned.add(foundBook);
			}
		}
		return returned.stream().sorted((b1, b2) -> {
			return b1.getAmountOfMoney().getAmount().compareTo(b2.getAmountOfMoney().getAmount());
		}).findFirst().get();
	}

	private Currency getCurrency() {
		try {
			return Currency.getInstance(parameters.getCurrency().get().toUpperCase());
		} catch (NullPointerException | NoSuchElementException e) {
			return Currency.getInstance(Locale.getDefault());
		}
	}

	private CheapestDealResponse bookForSaleNotFoundResponse() {
		return new CheapestDealResponse(Response.Status.NO_CONTENT, new Message(ResponseMessageType.INFO_NOT_FOUND_CHEAPEST_BOOK));
	}

	private CheapestDealResponse booksByGivenTitleNotFoundResponse() {
		return new CheapestDealResponse(Response.Status.NO_CONTENT, new Message(ResponseMessageType.INFO_BOOKS_NOT_FOUND_BY_TITLE));
	}

	private CheapestDealResponse wrongParametersResponse() {
		if (noTitle()) {
			return new CheapestDealResponse(Response.Status.BAD_REQUEST, new Message(ResponseMessageType.ERROR_NO_TITLE));
		} else if (wrongCurrency()) {
			return new CheapestDealResponse(Response.Status.NOT_ACCEPTABLE, new Message(ResponseMessageType.ERROR_NOT_KNOWN_CURRENCY));
		}
		throw new RuntimeException();
	}

	private boolean wrongParameters() {
		return noTitle() || wrongCurrency();
	}

	private boolean wrongCurrency() {
		if (!parameters.getCurrency().isPresent()) {
			return false;
		}
		try {
			Currency choosenCurrency = Currency.getInstance(parameters.getCurrency().get().toUpperCase());
			return Arrays.stream(AvailableCurrency.values()).noneMatch(c -> c.getCurrency().equals(choosenCurrency));
		} catch (IllegalArgumentException e) {
			return true;
		}
	}

	private boolean noTitle() {
		return !parameters.getTitle().isPresent() || Strings.isNullOrEmpty(parameters.getTitle().get().replaceAll(" ", ""));
	}
}

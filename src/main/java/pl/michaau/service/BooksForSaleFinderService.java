package pl.michaau.service;

import pl.michaau.clientapi.ebay.EbaySearchApi;
import pl.michaau.clientapi.google.GoogleBooksVolumesApi;
import pl.michaau.domain.FoundBook;

import java.util.HashSet;
import java.util.Set;

class BooksForSaleFinderService {

	Set<FoundBook> findByIsbns(Set<String> isbns) {
		Set<FoundBook> foundBooks = new HashSet<>();
		EbaySearchApi ebaySearchApi = new EbaySearchApi();
		GoogleBooksVolumesApi googleBooksVolumesApi = new GoogleBooksVolumesApi();
		isbns.forEach(isbn -> {
			foundBooks.addAll(ebaySearchApi.findBooksByIsbn(isbn));
			foundBooks.addAll(googleBooksVolumesApi.findBooksByIsbn(isbn));
		});
		return foundBooks;
	}
}


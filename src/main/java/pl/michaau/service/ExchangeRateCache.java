package pl.michaau.service;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Maps;
import lombok.*;
import pl.michaau.clientapi.exchangerates.ExchangeRatesApi;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Currency;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ExchangeRateCache {

	private static final LoadingCache<ExchangeRateKey, Map<Currency, BigDecimal>> EXCHANGE_RATES;

	static {
		EXCHANGE_RATES = CacheBuilder.newBuilder()
				.build(new CacheLoader<ExchangeRateKey, Map<Currency, BigDecimal>>() {

					@Override public Map<Currency, BigDecimal> load(ExchangeRateKey exchangeRateKey) {
						return new ExchangeRatesApi(exchangeRateKey.getCurrency()).getRates();
					}
				});

	}

	public static Map<Currency, BigDecimal> getExchangeRateForToday(Currency currency) {
		try {
			return EXCHANGE_RATES.get(new ExchangeRateKey(currency, LocalDate.now()));
		} catch (ExecutionException e) {
			return Maps.newHashMap();
		}
	}
}

@EqualsAndHashCode
@AllArgsConstructor
class ExchangeRateKey {

	@Getter
	private Currency currency;
	private LocalDate date;

}


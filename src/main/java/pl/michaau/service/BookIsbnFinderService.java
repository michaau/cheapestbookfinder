package pl.michaau.service;

import pl.michaau.clientapi.google.GoogleBooksVolumesApi;

import java.util.Set;

class BookIsbnFinderService {

	Set<String> findByTitle(String query) {
		return new GoogleBooksVolumesApi().findIsbnsByQuery(query);
	}
}

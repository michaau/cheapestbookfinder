package pl.michaau;

import pl.michaau.domain.CheapestBookToFindParameters;
import pl.michaau.domain.CheapestDealResponse;
import pl.michaau.service.CheapestBookFinderService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;

@Path("search-cheapest")
public class CheapestBookResource {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCheapestDeal(@QueryParam("title") String title, @QueryParam("currency") String currency) {
		return getResponse(new CheapestBookFinderService(CheapestBookToFindParameters.builder()
				.title(Optional.ofNullable(title))
				.currency(Optional.ofNullable(currency))
				.build()).find());
	}

	private Response getResponse(CheapestDealResponse result) {
		return Response.ok(result).build();

	}
}


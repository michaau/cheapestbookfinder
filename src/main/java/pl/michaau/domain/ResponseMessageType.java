package pl.michaau.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ResponseMessageType {

    INFO_BOOKS_NOT_FOUND_BY_TITLE(1L, "Not found any books by given title"),
    INFO_NOT_FOUND_CHEAPEST_BOOK(2L, "Not found any books for sale"),
    ERROR_NO_TITLE(1L,"Required parameter title is empty"),
    ERROR_NOT_KNOWN_CURRENCY(2L, "Not known currency");

    private Long codeId;
    private String message;


}

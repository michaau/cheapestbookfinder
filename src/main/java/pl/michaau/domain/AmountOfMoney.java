package pl.michaau.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import pl.michaau.service.ExchangeRateCache;

import java.math.BigDecimal;
import java.util.Currency;

@AllArgsConstructor
public class AmountOfMoney {

	@Getter
	private BigDecimal amount;
	@Getter
	private Currency currency;


	public AmountOfMoney convertTo(Currency currency) {
		return new AmountOfMoney(ExchangeRateCache.getExchangeRateForToday(this.currency).get(currency).multiply(this.amount), currency);
	}
}

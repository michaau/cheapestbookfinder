package pl.michaau.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

import java.util.Optional;

@Builder
@Getter
public class CheapestBookToFindParameters {

	@NonNull
	private Optional<String> title;
	@Builder.Default
	private Optional<String> currency = Optional.empty();
}

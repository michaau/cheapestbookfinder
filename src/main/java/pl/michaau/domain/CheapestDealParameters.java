package pl.michaau.domain;

import lombok.Builder;
import lombok.NonNull;

import java.util.Currency;
import java.util.Locale;
import java.util.Optional;

@Builder
public class CheapestDealParameters {

	@NonNull
	private String title;
	@Builder.Default
	private Optional<Currency> currencyOpt = Optional.of(Currency.getInstance(Locale.getDefault()));
}

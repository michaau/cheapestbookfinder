package pl.michaau.domain;

import pl.michaau.domain.Message;

import javax.ws.rs.core.Response;

public class CheapestDealResponse {

	private int status;
	private Message message;
	private String bookUrl;

	public CheapestDealResponse(Response.Status status, Message message) {
		this.status = status.getStatusCode();
		this.message = message;
	}

	public CheapestDealResponse(String bookUrl) {
		status = Response.Status.OK.getStatusCode();
		this.bookUrl = bookUrl;
	}
}

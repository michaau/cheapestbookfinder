package pl.michaau.domain;

public class Message {

    private Long code;
    private String message;

    public Message(ResponseMessageType type) {
        code = type.getCodeId();
        message = type.getMessage();
    }

}

package pl.michaau.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class FoundBook {

	@Getter
	private String bookLink;
	@Getter
	private AmountOfMoney amountOfMoney;

}

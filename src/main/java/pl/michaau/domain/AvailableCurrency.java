package pl.michaau.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Currency;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@AllArgsConstructor
public enum AvailableCurrency {
	PLN(Currency.getInstance("PLN")),
	GBP(Currency.getInstance("GBP")),
	EUR(Currency.getInstance("EUR")),
	USD(Currency.getInstance("USD")),
	CAD(Currency.getInstance("CAD"));

	@Getter
	private Currency currency;

	public static Set<AvailableCurrency> getAllWithout(Currency currency) {
		return Arrays.stream(AvailableCurrency.values()).filter(c -> !c.getCurrency().equals(currency)).collect(Collectors.toSet());
	}

}

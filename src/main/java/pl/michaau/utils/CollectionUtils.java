package pl.michaau.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Collection;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CollectionUtils {

	public static boolean notEmpty(Collection<?> collection) {
		return collection != null && !collection.isEmpty();
	}

}

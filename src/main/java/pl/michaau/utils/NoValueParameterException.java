package pl.michaau.utils;

public class NoValueParameterException extends Exception {

	public NoValueParameterException(String message) {
		super(message);
	}
}

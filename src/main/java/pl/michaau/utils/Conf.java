package pl.michaau.utils;

import com.google.common.base.Strings;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidParameterException;
import java.util.Optional;
import java.util.Properties;

public class Conf extends Properties {

	public static final String EBAY_API_KEY = "ebay_api_key";
	private static Properties properties;

	static {
		try {
			initialize();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void initialize() throws IOException {
		try (InputStream resourceStream = Conf.class.getClassLoader().getResourceAsStream("META-INF/application.properties")) {
			properties = new Properties();
			properties.load(resourceStream);
		}
	}

	public static String get(String param) throws NoValueParameterException {
		if (properties == null) {
			try {
				initialize();
				return property(param);
			} catch (IOException e) {
			}
		}
		return property(param);
	}

	private static String property(String param) throws NoValueParameterException {
		String property = properties.getProperty(param);
		if (Strings.isNullOrEmpty(property)) {
			throw new NoValueParameterException(param + " cound not be found in application parameters");
		}
		return property;
	}
}

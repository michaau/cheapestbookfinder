package pl.michaau.clientapi;

import pl.michaau.clientapi.utils.Mapper;
import pl.michaau.clientapi.utils.MessageReader;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Map;

public abstract class RestClient {

	protected String target;
	private static Client client;

	protected <T> T sendGET(Class<T> responseType, Map<String, String> parameters) {
		Response response = sendGET(parameters);
		return response.readEntity(responseType);
	}

	static {
		client = ClientBuilder.newBuilder()
				.register(Mapper.class)
				.register(MessageReader.class)
				.build();
	}

	private Response sendGET(Map<String, String> parameters) {
		WebTarget wt = client.target(getTarget());

		for (Map.Entry<String, String> parameter : parameters.entrySet()) {
			wt = wt.queryParam(parameter.getKey(), parameter.getValue());
		}
		return wt
				.request(MediaType.APPLICATION_JSON)
				.get();
	}

	protected abstract String getTarget();
}

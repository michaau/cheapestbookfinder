package pl.michaau.clientapi.google;

import lombok.extern.jbosslog.JBossLog;
import pl.michaau.clientapi.BookInShopSearcher;
import pl.michaau.clientapi.BookIsbnFinder;
import pl.michaau.clientapi.RestClient;
import pl.michaau.clientapi.google.domain.SearchByIsbnResponse;
import pl.michaau.clientapi.google.domain.SearchByQueryResponse;
import pl.michaau.domain.FoundBook;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@JBossLog
public class GoogleBooksVolumesApi extends RestClient implements BookIsbnFinder, BookInShopSearcher {

	public Set<String> findIsbnsByQuery(String query) {
		Map<String, String> parameters = new LinkedHashMap<>();
		parameters.put("q", query);
		SearchByQueryResponse response = sendGET(SearchByQueryResponse.class, parameters);
		return response.isbnsOf(query);
	}

	@Override public Set<FoundBook> findBooksByIsbn(String isbn) {
		Map<String, String> parameters = new LinkedHashMap<>();
		parameters.put("q", ":isbn" + isbn);
		SearchByIsbnResponse response = sendGET(SearchByIsbnResponse.class, parameters);
		return response.getBooks();
	}

	protected String getTarget() {
		return "https://www.googleapis.com/books/v1/volumes";
	}
}

package pl.michaau.clientapi.google.domain;

import pl.michaau.domain.AmountOfMoney;
import pl.michaau.domain.FoundBook;

import java.math.BigDecimal;
import java.util.Currency;

class SaleInfo {

	private String buyLink;
	private RetailPrice retailPrice;

	FoundBook getFoundBook() {
		try {
			return new FoundBook(buyLink.trim(), getAmount());
		} catch (IllegalArgumentException | NullPointerException e) {
			return null;
		}
	}

	private AmountOfMoney getAmount() {
		return new AmountOfMoney(new BigDecimal(retailPrice.getAmount()), Currency.getInstance(retailPrice.getCurrencyCode()));
	}
}

package pl.michaau.clientapi.google.domain;

import java.util.Collections;
import java.util.List;

class VolumeInfo {

	private String title;
	private List<IndustryIdentifier> industryIdentifiers;

	boolean isSameTitleAs(String title) {
		try {
			return this.title.equalsIgnoreCase(title);
		} catch (NullPointerException e) {
			return false;
		}
	}

	List<IndustryIdentifier> getIndustryIdentifiers(){
		return Collections.unmodifiableList(industryIdentifiers);
	}
}

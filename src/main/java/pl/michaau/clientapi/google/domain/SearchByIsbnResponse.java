package pl.michaau.clientapi.google.domain;

import pl.michaau.domain.FoundBook;
import pl.michaau.utils.CollectionUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class SearchByIsbnResponse {

	private List<Item> items;

	public Set<FoundBook> getBooks() {
		if (CollectionUtils.notEmpty(items)) {
			return items.stream()
					.map(Item::getSaleInfo)
					.filter(Objects::nonNull)
					.map(SaleInfo::getFoundBook)
					.filter(Objects::nonNull)
					.collect(Collectors.toSet());
		}
		return new HashSet<>();
	}
}

package pl.michaau.clientapi.google.domain;

import lombok.Getter;

class RetailPrice {

	@Getter
	private String amount;
	@Getter
	private String currencyCode;
}

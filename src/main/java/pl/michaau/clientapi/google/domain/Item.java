package pl.michaau.clientapi.google.domain;

import lombok.Getter;

@Getter
class Item {

	private VolumeInfo volumeInfo;
	private SaleInfo saleInfo;
}

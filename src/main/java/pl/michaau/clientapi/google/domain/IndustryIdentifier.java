package pl.michaau.clientapi.google.domain;

class IndustryIdentifier {

	private String type;
	private String identifier;

	boolean isISBN() {
		return "ISBN_10".equalsIgnoreCase(type) || "ISBN_13".equalsIgnoreCase(type);
	}

	String getISBN() {
		return identifier;
	}
}

package pl.michaau.clientapi.google.domain;

import com.google.common.collect.Sets;
import pl.michaau.utils.CollectionUtils;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class SearchByQueryResponse {

	private List<Item> items;
	private String title;

	public Set<String> isbnsOf(String title) {
		this.title = title;
		if (CollectionUtils.notEmpty(items)) {
			return items.stream()
					.map(Item::getVolumeInfo)
					.filter(this::isSameTitle)
					.map(VolumeInfo::getIndustryIdentifiers)
					.filter(CollectionUtils::notEmpty)
					.flatMap(List::stream)
					.filter(IndustryIdentifier::isISBN)
					.map(IndustryIdentifier::getISBN)
					.collect(Collectors.toSet());
		}
		return Sets.newHashSet();
	}

	private boolean isSameTitle(VolumeInfo volumeInfo) {
		try {
			return volumeInfo.isSameTitleAs(title);
		} catch (NullPointerException e) {
			return false;
		}
	}
}

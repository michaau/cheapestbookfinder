package pl.michaau.clientapi;

import pl.michaau.domain.FoundBook;

import java.util.Set;

public interface BookInShopSearcher {

	Set<FoundBook> findBooksByIsbn(String isbn);
}

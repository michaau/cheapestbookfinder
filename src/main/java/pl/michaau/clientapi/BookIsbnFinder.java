package pl.michaau.clientapi;

import java.util.Set;

public interface BookIsbnFinder {

	Set<String> findIsbnsByQuery(String query);
}

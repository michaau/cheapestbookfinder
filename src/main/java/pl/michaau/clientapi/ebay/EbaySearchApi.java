package pl.michaau.clientapi.ebay;

import com.google.common.collect.Sets;
import lombok.extern.jbosslog.JBossLog;
import pl.michaau.clientapi.BookInShopSearcher;
import pl.michaau.clientapi.RestClient;
import pl.michaau.clientapi.ebay.domain.Response;
import pl.michaau.domain.FoundBook;
import pl.michaau.utils.Conf;
import pl.michaau.utils.NoValueParameterException;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@JBossLog
public class EbaySearchApi extends RestClient implements BookInShopSearcher {

	public Set<FoundBook> findBooksByIsbn(String isbn) {
		try {
			Map<String, String> parameters = new LinkedHashMap<>();
			parameters.put("OPERATION-NAME", "findItemsByProduct");
			parameters.put("SERVICE-VERSION", "1.0.0");
			parameters.put("SECURITY-APPNAME", Conf.get(Conf.EBAY_API_KEY));
			parameters.put("RESPONSE-DATA-FORMAT", "JSON");
			parameters.put("productId.@type", "ISBN");
			parameters.put("productId", isbn);
			Response response = sendGET(Response.class, parameters);
			return response.getBooks();
		} catch (NoValueParameterException e) {
			log.info("Ebay Api Key not defined in application properties. Searching in ebay will be omitted");
			return Sets.newHashSet();
		}
	}

	@Override protected String getTarget() {
		return "http://svcs.ebay.com/services/search/FindingService/v1";
	}
}

package pl.michaau.clientapi.ebay.domain;

import lombok.Getter;

import java.util.List;

class ShippingInfo {

	@Getter
	private List<ShippingServiceCost> shippingServiceCost;
}

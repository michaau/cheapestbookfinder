package pl.michaau.clientapi.ebay.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
class CurrentPrice {

	@JsonProperty("@currencyId")
	private String currencyId;
	@JsonProperty("__value__")
	private String value;

}

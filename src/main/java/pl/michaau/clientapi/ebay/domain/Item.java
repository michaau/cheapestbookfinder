package pl.michaau.clientapi.ebay.domain;

import pl.michaau.domain.AmountOfMoney;
import pl.michaau.domain.FoundBook;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.List;

class Item {

	private List<String> viewItemURL;
	private List<ShippingInfo> shippingInfo;
	private List<SellingStatus> sellingStatus;

	FoundBook getFoundBook() {
		try {
			return new FoundBook(viewItemURL.get(0).trim(), getAmount());
		} catch (IndexOutOfBoundsException | NullPointerException | IllegalArgumentException e) {
			return null;
		}
	}

	private AmountOfMoney getAmount() {
		ShippingServiceCost shippingServiceCost = shippingInfo.get(0).getShippingServiceCost().get(0);
		CurrentPrice currentPrice = sellingStatus.get(0).getCurrentPrice().get(0);
		return new AmountOfMoney(new BigDecimal(shippingServiceCost.getValue()).add(new BigDecimal(currentPrice.getValue())), Currency.getInstance(currentPrice.getCurrencyId()));
	}
}

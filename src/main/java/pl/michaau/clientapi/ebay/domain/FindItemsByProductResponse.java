package pl.michaau.clientapi.ebay.domain;

import lombok.Getter;

import java.util.List;

class FindItemsByProductResponse {

	@Getter
	private List<SearchResult> searchResult;
}

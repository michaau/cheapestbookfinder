package pl.michaau.clientapi.ebay.domain;

import lombok.Getter;

import java.util.List;

class SearchResult {

	@Getter
	private List<Item> item;

}

package pl.michaau.clientapi.ebay.domain;

import lombok.Getter;

import java.util.List;

@Getter
class SellingStatus {

	private List<CurrentPrice> currentPrice;

}

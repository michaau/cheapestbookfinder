package pl.michaau.clientapi.ebay.domain;

import pl.michaau.domain.FoundBook;
import pl.michaau.utils.CollectionUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class Response {

	private List<FindItemsByProductResponse> findItemsByProductResponse;

	public Set<FoundBook> getBooks() {
		if (CollectionUtils.notEmpty(findItemsByProductResponse)) {
			return findItemsByProductResponse.stream()
					.map(FindItemsByProductResponse::getSearchResult)
					.filter(Objects::nonNull)
					.flatMap(List::stream)
					.map(SearchResult::getItem)
					.filter(Objects::nonNull)
					.flatMap(List::stream)
					.map(Item::getFoundBook)
					.filter(Objects::nonNull)
					.collect(Collectors.toSet());
		}
		return new HashSet<>();
	}
}

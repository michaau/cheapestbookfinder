package pl.michaau.clientapi.exchangerates.domain;

import com.google.common.collect.Maps;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Map;

public class ExchangeRatesResponse {

	private Map<String, String> rates;

	public Map<Currency, BigDecimal> getRatesMap() {
		Map<Currency, BigDecimal> resultRates = Maps.newHashMap();
		try {
			rates.forEach((key, value) -> resultRates.put(Currency.getInstance(key), new BigDecimal(value)));
			return resultRates;
		} catch (NullPointerException | IllegalArgumentException e) {
			return Maps.newHashMap();
		}
	}
}

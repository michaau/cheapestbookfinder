package pl.michaau.clientapi.exchangerates;

import pl.michaau.clientapi.RestClient;
import pl.michaau.clientapi.exchangerates.domain.ExchangeRatesResponse;
import pl.michaau.domain.AvailableCurrency;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class ExchangeRatesApi extends RestClient {

	private Currency currency;

	public ExchangeRatesApi(Currency currency) {
		this.currency = currency;
	}

	public Map<Currency, BigDecimal> getRates() {
			Map<String, String> parameters = new LinkedHashMap<>();
			parameters.put("base", currency.getCurrencyCode());
			parameters.put("symbols", AvailableCurrency.getAllWithout(currency).stream()
					.map(AvailableCurrency::getCurrency)
					.map(Currency::getCurrencyCode)
					.collect(Collectors.joining(",")));
			ExchangeRatesResponse response = sendGET(ExchangeRatesResponse.class, parameters);
			return response.getRatesMap();
	}

	@Override protected String getTarget() {
		return "https://api.exchangeratesapi.io/latest";
	}
}
